package com.spring.hateoas;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SpringHateoasApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testGetProduct() throws Exception {
		mockMvc.perform(get("/products/1")).andExpect(jsonPath("$.code").value("1"))
				.andExpect(jsonPath("$.name").value("Sample Product")).andExpect(jsonPath("$.price").value(100));
	}

	@Test
	public void testGetProducts() throws Exception {
		mockMvc.perform(get("/products")).andExpect(jsonPath("links[0].rel", is("products")))
				.andExpect(jsonPath("links[0].href", is("http://localhost/products")))
				.andExpect(jsonPath("content[0].code", is("1")))
				.andExpect(jsonPath("content[0].links[0].rel", is("review")))
				.andExpect(jsonPath("content[0].links[0].href", is("http://localhost/products/1/reviews")));
	}

}
