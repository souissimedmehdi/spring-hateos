package com.spring.hateoas.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;

@EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
public class CollectionJsonApplication {

}