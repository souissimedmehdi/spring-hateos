package com.spring.hateoas.data;

import org.springframework.hateoas.ResourceSupport;

import lombok.Data;

@Data
public class Product extends ResourceSupport {

    private String code;
    private String name;
    private String description;
    private double price;
    private String categoryId;
}
