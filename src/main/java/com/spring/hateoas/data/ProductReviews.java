package com.spring.hateoas.data;

import org.springframework.hateoas.ResourceSupport;

import lombok.Data;

@Data
public class ProductReviews extends ResourceSupport {

    private String code;
    private String productCode;
    private double score;

}
