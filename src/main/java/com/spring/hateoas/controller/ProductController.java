package com.spring.hateoas.controller;

import com.spring.hateoas.data.Product;
import com.spring.hateoas.data.ProductReviews;
import com.spring.hateoas.services.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Resources<Product> getProducts() {

        List<Product> products = productService.getProducts();
        products.forEach(product -> {
        	Link selfLink = linkTo(methodOn(ProductController.class)
                    .getProduct(product.getCode())).withRel("product");
            Link productReview = linkTo(methodOn(ProductController.class)
                    .getReviewsForProduct(product.getCode())).withRel("review");
            product.add(productReview);
            product.add(selfLink);
        }); 
        
        Link link = linkTo(methodOn(ProductController.class)
                .getProducts()).withRel("products");
        return new Resources<>(products, link);
    }

    @GetMapping("/{productId}")
    public Product getProduct(@PathVariable final String productId) {

        Product product = productService.getProductById(productId);
        product.add(linkTo(ProductController.class).slash(product.getCode()).withSelfRel());
        product.add(linkTo(methodOn(ProductController.class).getReviewsForProduct(productId)).withRel("reviews"));
        return product;
    }

    @GetMapping(value = "/{productId}/reviews", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Resources<ProductReviews> getReviewsForProduct(@PathVariable final String productId) {

        List<ProductReviews> reviews = productService.getProductReviews(productId);
        for (final ProductReviews review : reviews) {
            Link selfLink = linkTo(methodOn(ProductController.class)
                    .getProduct(productId)).withSelfRel();
            review.add(selfLink);
        }

        Link link = linkTo(methodOn(ProductController.class)
                .getReviewsForProduct(productId)).withRel("reviews");
        return new Resources<>(reviews, link);

    }
}
