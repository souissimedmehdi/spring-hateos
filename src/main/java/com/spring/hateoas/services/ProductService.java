package com.spring.hateoas.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

import com.spring.hateoas.data.Product;
import com.spring.hateoas.data.ProductReviews;

@Service
public class ProductService {

	public List<ProductReviews> getProductReviews(final String productCode) {
		List<ProductReviews> productReviews = new ArrayList<>();
		IntStream.rangeClosed(0, 2).forEach(i -> {
			ProductReviews reviews = new ProductReviews();
			reviews.setCode(String.valueOf(i));
			reviews.setProductCode(productCode);
			reviews.setScore(i);
			productReviews.add(reviews);
		});
		return productReviews; 
	}

	public List<Product> getProducts() {
		List<Product> products = new ArrayList<>();
		IntStream.rangeClosed(1, 3).forEach(i -> {
			Product product = new Product();
			product.setCode(String.valueOf(i));
			product.setName("Product" + i);
			product.setDescription("This is a demo product number :" + i);
			product.setCategoryId("cat_" + i);
			product.setPrice(i * 12.67);
			products.add(product);
		});
		return products;
	}

	public Product getProductById(final String code) {
		Product product = new Product();
		product.setCode(code);
		product.setName("Sample Product");
		product.setDescription("This is our first sample product");
		product.setPrice(100.00);
		product.setCategoryId("Cat1");
		return product;
	}
}
